<!DOCTYPE html>
<html lang="fr">
    <head>
        <meta charset="utf-8">
        <title>Chat Game</title>
        <link href="bootstrap.min.css" rel="stylesheet">
        <link rel="stylesheet" href="style.css" type="text/css" media="all" />
    </head>
    <body class="text-center">
        <div id="users">
        </div>

        <div id="chat_box">
            <div class="container p-0" >
                <svg width="6em" height="6em" viewBox="0 0 16 16" class="bi bi-chat-dots" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                    <path fill-rule="evenodd" d="M2.678 11.894a1 1 0 0 1 .287.801 10.97 10.97 0 0 1-.398 2c1.395-.323 2.247-.697 2.634-.893a1 1 0 0 1 .71-.074A8.06 8.06 0 0 0 8 14c3.996 0 7-2.807 7-6 0-3.192-3.004-6-7-6S1 4.808 1 8c0 1.468.617 2.83 1.678 3.894zm-.493 3.905a21.682 21.682 0 0 1-.713.129c-.2.032-.352-.176-.273-.362a9.68 9.68 0 0 0 .244-.637l.003-.01c.248-.72.45-1.548.524-2.319C.743 11.37 0 9.76 0 8c0-3.866 3.582-7 8-7s8 3.134 8 7-3.582 7-8 7a9.06 9.06 0 0 1-2.347-.306c-.52.263-1.639.742-3.468 1.105z"/>
                    <path d="M5 8a1 1 0 1 1-2 0 1 1 0 0 1 2 0zm4 0a1 1 0 1 1-2 0 1 1 0 0 1 2 0zm4 0a1 1 0 1 1-2 0 1 1 0 0 1 2 0z"/>
                </svg>
                <h1 class="h3 mb-3">Messagerie</h1>
                
                <!-- div pour bouton déconnecté -->
                    <form class="input-group" id="Déconnexion">
                        <button class="btn btn-danger " type="submit">Déconnexion</button>
                    </form>
                    <p></p>
                    <form class="input-group" id="Quitter">
                        <button class="btn btn-danger " type="submit">Quitter le chat</button>
                    </form>

                <!-- div pour bouton salon -->
                <div class="d-flex flex-row">
                    <form class="p-2" id="salon1">
                        <button  type="submit" class="btn btn-dark">Salon Global</button>
                    </form>
                    <form class="p-2" id="salon2">
                        <button type="submit" class="btn btn-dark">Salon Privé</button>
                    </form>
                    <!-- On a d'autres bouttons car nous avons créé l'appli de chat avant d'insérer la partie "jeu" ainsi on pourrait choisir nous-memes nos salons-->
                    <!--<form class="p-2" id="salon3">
                        <button type="submit" class="btn btn-dark">Salon 3</button>
                    </form>
                    <form class="p-2" id="salon4">
                        <button type="submit" class="btn btn-dark">Salon 4</button>
                    </form>-->
                </div>

                <div class="card">
                    <div class="row g-0">
                        
                        <!-- div pour afficher la conlone à gauche du chat -->

                        <div id="person_co" class="col-12 col-lg-5 col-xl-3 border-right">

                            <!-- div pour chercher un personne connectée -->

                            <div class="px-4 d-none d-md-block">
                                <div class="d-flex align-items-center">
                                    <div class="d-flex flex-row">
                                        <svg width="3em" height="3em" viewBox="0 0 16 16" class="bi bi-person-check" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                            <path fill-rule="evenodd" d="M8 5a2 2 0 1 1-4 0 2 2 0 0 1 4 0zM6 8a3 3 0 1 0 0-6 3 3 0 0 0 0 6zm6 5c0 1-1 1-1 1H1s-1 0-1-1 1-4 6-4 6 3 6 4zm-1-.004c-.001-.246-.154-.986-.832-1.664C9.516 10.68 8.289 10 6 10c-2.29 0-3.516.68-4.168 1.332-.678.678-.83 1.418-.832 1.664h10zm4.854-7.85a.5.5 0 0 1 0 .708l-3 3a.5.5 0 0 1-.708 0l-1.5-1.5a.5.5 0 0 1 .708-.708L12.5 7.793l2.646-2.647a.5.5 0 0 1 .708 0z"/>
                                        </svg>
                                        <h5>Personnes connectées : </h5>
                                    </div>
                                </div>
                            </div>

                            <!-- div pour afficher des personnes connectées dans la colone sur le coté droit -->
                                 <!-- du code HTML est ajouté ici dés que quelqu'un ce connect -->

                            <hr class="d-block d-lg-none mt-1 mb-0">

                        </div>

                        <!-- div pour afficher au dessus de la chat la chat-box principale qui est le destinataire du message -->
                           
                        <div class="col-12 col-lg-7 col-xl-9">

                            <!-- div pour afficher les msgs dans la chatbox -->
                            <div class="position-relative" id="messagesContent">

                            </div>

                            <!-- div pour envoyer un message  -->

                            <div class="flex-grow-0 py-3 px-4 border-top">
                                <form class="input-group" id=sendForm>
                                    <input type="text" id="msgInput" class="form-control" placeholder="Entrez votre message">
                                    <button class="btn btn-primary" type="submit">Envoyer</button>
                                </form>
                            </div>

                        </div>
                    </div>
                </div>

            </div>
        </div>

        <form class="loginform" id="loginform">
            <!-- Ici on a la description html de notre page de login-->
            <h1 class="h3 mb-3 font-weight-normal">Veuillez vous identifier</h1>
            <label for="username" class="sr-only">Nom d'utilisateur</label>
            <input type="text" name="login" id="username" class="form-control" placeholder="Nom d'utilisateur" required autofocus>
            <button class="btn btn-lg btn-primary btn-block" type="submit">Se Connecter</button>
            <p></p>
            <form>
            <input type="button" id="inputPikaRed" value="Pikachu">
            <p id="pikaRed">Vous avez le skin de Red !</p>
            <img id="imgSkin" val="RedAnime.png" src="Red.png"/>
            <p class="mt-5 mb-3 text-muted">&copy; developpé par Charles & Benjamin</p>
            </form>
        </form>

        <div class="canvasLayerList" id="canvasLayerList">
            <!-- description html de notre page de jeu-->
            <canvas class="character" id="character" width="1280" height="720"></canvas>
            <canvas class="backgroundart" id="backgroundart" width="1280" height="720"></canvas>
        </div>

        <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
        <script src="/socket.io/socket.io.js"></script>
        <script> //lancement du script javascript html
//----------------------------------------------------------------------------------------
// Permet d'afficher soit la chatbox soit l'écran de login en fonction de l'état de connexion de l'utilisateur
        document.getElementById("users").style.display = "none";
        document.getElementById("chat_box").style.display = "none";
        document.getElementById("canvasLayerList").style.display = "none";

        function login_to_chat(){
            //document.getElementById("users").style.display = "block"; 
            //document.getElementById("chat_box").style.display = "block"; 
            document.getElementById("canvasLayerList").style.display = "block"
            document.getElementById("loginform").style.display = "none"; 
        };
//----------------------------------------------------------------------------------------

        //cette partie javascript sert à changer le boutton et l'image du choix de personnage
        var btn = document.getElementById('inputPikaRed');
        var txt = document.getElementById('pikaRed');
        var img = document.getElementById('imgSkin');
        var skinchoice="RedAnime.png";

        btn.addEventListener('click', updateBtn);

        function updateBtn() {
            if (btn.value === 'Pikachu') {
                btn.value = 'Red';
                txt.textContent = 'Vous avez le skin de Pikachu !';
                img.src = 'Pika.png';
                img.val = 'pikanimx4.png';
                skinchoice = "pikanimx4.png"; //si on choisit le boutton pikachu on aura l'avatar pikachu
            } else {
                btn.value = 'Pikachu';
                txt.textContent = 'Red';
                img.src = 'Red.png';
                img.val = 'RedAnime.png';
                skinchoice = "RedAnime.png"; //le contraire pour red
            }
        }


        $(function() {
            var socket = io()
            var currentUser;
            var rg;
            var messages1_histo1;
            var users1_histo1;
            var date1_histo1;
            var messages2_histo2;
            var users2_histo2;
            var date2_histo2;
            var messages3_histo3;
            var users3_histo3;
            var date3_histo3;
            var messages4_histo4;
            var users4_histo4;
            var date4_histo4;
            var current_salon=0;
            var user_listloc = [];
            var posX_listloc = [];
            var posY_listloc = [];
            var skin_listloc = [];
            var newchat = 1;
            var chatsave = [];
            var chatreverse = [];
            var boolchat = false;
//---------------------------------------------------------------------------------------- 
            //variables pour la création visuelle du jeu
            var tID;
            var background = document.getElementById("backgroundart");
            var BGcontext = background.getContext("2d");
            var character = document.getElementById("character");
            var Characontext = character.getContext("2d");

            var canvasDim = {
                width: background.width,
                height: background.height
            };

            //on définit à quel endroit y du sprite on doit se placer en fonction de la touche appuyée
            var direction = {
                "ArrowUp": 0,
                "ArrowDown": 140,
                "ArrowLeft": 280,
                "ArrowRight": 420
            };

            //initialisations des valeurs de base
            var sy = direction["ArrowUp"];
            var sx = 0;
            var px = 0;
            var py = 0;
            var x = 0;
            var sprite = {
                city: null
            }

            var spriteLoaded = function () {

                this.loaded = true;

                for (let name in sprite) {
                    if (!sprite[name].loaded) {
                        return;
                    }
                }
                init();
            };

            //chargement sprite background
            sprite.city = new Image();
            sprite.city.src = "background.png";
            sprite.city.onload = spriteLoaded;

            function init() {
                for (let l = 0; l * 2800 <= canvasDim.height; l++) {
                    for (let c = 0; c * 1600 <= canvasDim.width; c++) {
                        BGcontext.drawImage(sprite.city, c * 2800, l * 1600, 1280, 720);
                        animateScript();
                    }
                }
            }

            //fonction qui servira à dessiner le personnage à chaque fois qu'on en aura besoin
            var drawPerso = function () {
                for(x=0; x<user_listloc.length; x++) {
                    Characontext.drawImage(perso, sx, sy, 140, 140, posX_listloc[x], posY_listloc[x], 140, 140);
                    Characontext.fillText(user_listloc[x], posX_listloc[x] + 38 - user_listloc[x].length * 2, posY_listloc[x]-5);
                    Characontext.font = "bold 24px verdana, sans-serif"
                    Characontext.fillStyle = "#FFFFFF";
                }
            };

            var animateScript = function () {
                var position = 0;
                const diff = 140;
                const interval = 200;

                //mise en place d'un timer pour pouvoir changer de position sur le sprite tout les tant de temps (=animation du sprite)
                tID = setInterval(() => {
                    sx = position;
                    if (sy < 280) {
                        if (position < 560) {
                            position = position + diff;
                        }
                        else {
                            position = 0;
                        }
                    }
                    else {
                        if (position < 280) {
                            position = position + diff;
                        }
                        else {
                            position = 0;
                        }
                    }
                    Characontext.clearRect(0, 0, background.width, background.height);

                    drawPerso();
                }, interval);
            }

            var perso = new Image();
            perso.src = "RedAnime.png"; //nous n'avons pas pu implémenter le changement de personnage jusqu'au bout (problème de récupération d'information value depuis une image)
            perso.height = 280;
            perso.width = 350;
            perso.onload = animateScript;

            //action à réaliser à chaque appui d'une touche
            document.onkeydown = function (e) {
                //on applique l'action seulement si elle fait partie de celles définie (4 touches directionelles)
                if (direction[e.key] === undefined) {
                    return;
                }
                sy = direction[e.key];
                var ppx = px; //ancienne position pour clear le canva
                var ppy = py;
                if (sy == 420) { px = px + 10; } else if (sy == 280) { px = px - 10; } else if (sy == 0) { py = py - 10; } else if (sy == 140) { py = py + 10; } //on applique la nouvelle position (10px par 10px)
                //console.log(rg);
                posX_listloc[rg] = px;
                posY_listloc[rg] = py;
                Characontext.clearRect(0, 0, background.width, background.height);
                skin_listloc[rg] = skinchoice;
                socket.emit("coord_refresh", posX_listloc, posY_listloc, skin_listloc); //on emit un update de la liste de coordonnées
            };
            animateScript();
//---------------------------------------------------------------------------------------- 
            //lorsque les coordonnées sont update on peut update le fichier template une dernière fois et dessiner le personnage
            socket.on("coord_update", (posX_list, posY_list, users_list) =>{
                user_listloc = users_list;
                posX_listloc = posX_list;
                posY_listloc = posY_list;
                drawPerso();
            });
//---------------------------------------------------------------------------------------- 
            //a la connection on lie toutes les informations du coté serveur au coté client
            socket.on("connected", (users_list, posX_list, posY_list, rang, skin_list) =>{
                    user_listloc = users_list;
                    rg = rang;
                    posX_listloc = posX_list;
                    posY_listloc = posY_list;
                    skin_listloc = skin_list;
                    console.log(skin_listloc[rg]);
                    console.log(user_listloc[rg]);
            });
//---------------------------------------------------------------------------------------- 
            //lorsque 2 personnes sont disposées à chat on affiche la page correspondante
            socket.on("chat", (chat) => {
                document.getElementById("users").style.display = "block"; 
                document.getElementById("chat_box").style.display = "block";
                document.getElementById("canvasLayerList").style.display = "none";
                //console.log("id chat: " + chat + " id save: " + chatsave + " idreverse: "+ chatreverse + " newchat: " + newchat);

                //nous voulions mettre en place un système de salons où à chaque nouvelle personne croisée on join un autre salon (jusqu'à 4) sauf si on l'a déjà croisée
                //mais nous avons fait face à des problèmes (une incapacité à reverse la liste de rang pour pouvoir vérifier dans les deux sens car en fonction de quel personne colle l'autre en 1er cela peut changer l'ordre de la liste)
                //je ne comprends toujours pas à ce jour pourquoi je n'arrive pas à reverse chatreverse et reverse chat à la place
                //en corrigeant ce problème on peut utiliser le système de chat privé au mieux
                if(chat != chatsave && chat != chatreverse && !boolchat){
                    newchat++;
                    boolchat = true;
                }
                //console.log("id chat: " + chat + " id save: " + chatsave + " idreverse: "+ chatreverse + " newchat: " + newchat);
                chatsave = chat;
                chatreverse = chat;
                let save;
                save = chatreverse[0];
                chatreverse[0] = chatreverse[1];
                chatreverse[1] = save;
                //console.log("id chat: " + chat + " id save: " + chatsave + " idreverse: "+ chatreverse + " newchat: " + newchat);
            });
//---------------------------------------------------------------------------------------- 
            //losqu'on submit une connexion on réalise ce code
            $('#loginform').submit(function(e) {
                e.preventDefault(); // prevents page reloading
                socket.emit('login', $('#username').val());
                //console.log(skinchoice);
                me=$('#username').val();
                currentUser=me;
                alert(me+" vous etes bien connecté")
                login_to_chat();
//----------------------------------------------------------------------------------------
// permet de supprimer l'utilisateur qui s'est deconnecté
                socket.on("rg_deconnecté", function(aff){
                    var rg_deco = aff.rang;
                    var nom = aff.list;
                    alert("Vous vous êtes déconnecté")
                });

                socket.on("rg_deconnecté2", function(aff){
                    var rg_deco = aff.rang;
                    var nom = aff.list;
                    alert(nom+" s'est déconnecté");
                    $('#'+rg_deco).remove();
                });

//----------------------------------------------------------------------------------------
// Permet d'ajouter a la list d'untilisateur connecté un nouvelle utilisteur lorsque celui-ci se connect

                socket.on('userlist_refresh2', function(aff){
                var rg_co = aff.rg;
                var user_list = aff.list;
                alert(user_list[user_list.length-1]+' vient de se connecter');
                $('#person_co').append('<a id='+rg_co+' href="#" class="list-group-item list-group-item-action border-0"><div class="d-flex align-items-start"><img src="https://www.icone-png.com/png/3/2625.png" class="rounded-circle mr-1" alt="'+user_list[user_list.length-1]+'" width="40" height="40"><div class="flex-grow-1 ml-3">'+user_list[user_list.length-1]+'<div class="small"><span class="fas fa-circle chat-online"></span> Online</div></div></div></a>')
                });

                socket.on('userlist_refresh', function(aff){
                    var rg_refr = aff.rg;
                    var user_list = aff.list;
                    for(var i in user_list){
                        if (user_list[i]!=null){
                            $('#person_co').append('<a id='+i+' href="#" class="list-group-item list-group-item-action border-0"><div class="d-flex align-items-start"><img src="https://www.icone-png.com/png/3/2625.png" class="rounded-circle mr-1" alt="'+user_list[i]+'" width="40" height="40"><div class="flex-grow-1 ml-3">'+user_list[i]+'<div class="small"><span class="fas fa-circle chat-online"></span> Online</div></div></div></a>')
                        }
                    }
                });

//---------------------------------------------------------------------------------------- 
// Envoie le rg de l'utilisateur qui s'est deconnecté lorsque que celui-ci appuie sur le boutton deconnexion

                $('#Déconnexion').submit(function(e) {
                    socket.emit('deconnect', rg);
                });

                $('#Quitter').submit(function(e) {
                    e.preventDefault();
                    document.getElementById("users").style.display = "none"; 
                    document.getElementById("chat_box").style.display = "none"; 
                    boolchat = false;
                    login_to_chat();
                    //newchat = 1;
                });
//----------------------------------------------------------------------------------------
// Historisation des messages du salon 1 lorsque quelqu'un de nouveau se connect
// Réponse du ask1 par le serveur il nous envoie les infos d'historiques du salon et on les affiche
                socket.on("message_histo1",  (salon1)=> {
                    messages1_histo1=salon1[0];
                    users1_histo1=salon1[1];
                    date1_histo1=salon1[2];

                    for(var i in messages1_histo1){
                        if (messages1_histo1[i]!=0){
                                $("#messagesContent").append('<div class="chat-message-left pb-4"><div><img src="https://www.icone-png.com/png/3/2625.png" class="rounded-circle mr-1" alt="Sharon Lessman" width="40" height="40"><div class="text-muted small text-nowrap mt-2">'+date1_histo1[i]+'</div></div><div class="flex-shrink-1 bg-light rounded py-2 px-3 ml-3"><div class="font-weight-bold mb-1">'+users1_histo1[i]+'</div>'+messages1_histo1[i]+'</div></div>')                            
                        }
                    }
                });
//----------------------------------------------------------------------------------------
// Historisation des messages du salon 2 lorsque quelqu'un de nouveau se connect

                socket.on("message_histo2",  (salon2)=> {
                    messages2_histo2=salon2[0];
                    users2_histo2=salon2[1];
                    date2_histo2=salon2[2];   
                    for(var i in messages2_histo2){
                        if (messages2_histo2[i]!=0){
                                $("#messagesContent").append('<div class="chat-message-left pb-4"><div><img src="https://www.icone-png.com/png/3/2625.png" class="rounded-circle mr-1" alt="Sharon Lessman" width="40" height="40"><div class="text-muted small text-nowrap mt-2">'+date2_histo2[i]+'</div></div><div class="flex-shrink-1 bg-light rounded py-2 px-3 ml-3"><div class="font-weight-bold mb-1">'+users2_histo2[i]+'</div>'+messages2_histo2[i]+'</div></div>')                            
                        }
                    }
                });
//----------------------------------------------------------------------------------------
// Historisation des messages du salon 3 lorsque quelqu'un de nouveau se connect

                socket.on("message_histo3",  (salon3)=> {
                    messages3_histo3=salon3[0];
                    users3_histo3=salon3[1];
                    date3_histo3=salon3[2];  

                    for(var i in messages3_histo3){
                        if (messages3_histo3[i]!=0){
                                $("#messagesContent").append('<div class="chat-message-left pb-4"><div><img src="https://www.icone-png.com/png/3/2625.png" class="rounded-circle mr-1" alt="Sharon Lessman" width="40" height="40"><div class="text-muted small text-nowrap mt-2">'+date3_histo3[i]+'</div></div><div class="flex-shrink-1 bg-light rounded py-2 px-3 ml-3"><div class="font-weight-bold mb-1">'+users3_histo3[i]+'</div>'+messages3_histo3[i]+'</div></div>')                            
                        }
                    }
                });
 //----------------------------------------------------------------------------------------
// Historisation des messages du salon 4 lorsque quelqu'un de nouveau se connect

                socket.on("message_histo4",  (salon4)=> {
                    messages4_histo4=salon4[0];
                    users4_histo4=salon4[1];
                    date4_histo4=salon4[2]; 

                    for(var i in messages4_histo4){
                        if (messages4_histo4[i]!=0){
                                $("#messagesContent").append('<div class="chat-message-left pb-4"><div><img src="https://www.icone-png.com/png/3/2625.png" class="rounded-circle mr-1" alt="Sharon Lessman" width="40" height="40"><div class="text-muted small text-nowrap mt-2">'+date4_histo4[i]+'</div></div><div class="flex-shrink-1 bg-light rounded py-2 px-3 ml-3"><div class="font-weight-bold mb-1">'+users4_histo4[i]+'</div>'+messages4_histo4[i]+'</div></div>')                            
                        }
                    }
                });

//----------------------------------------------------------------------------------------
// Pour envoyer un message dans le bon salon de chat
//En fonction du salon choisit on affiche le message entré dans la chatbox et on l'envoie au serveur

                $('#sendForm').submit(function(e) {
                    var date = new Date();
                    var mois=date.getMonth()+1; // On ajoute 1 au mois pour l'affichage car janvier est le mois 0
                    var strDate = "Le "+date.getDate()+"/"+mois+" à "+date.getHours() + ":" + date.getMinutes();
                   
                    switch (current_salon){
                        case 1:
                            e.preventDefault();
                            msg=$('#msgInput').val();
                            $("#messagesContent").append('<div class="chat-message-right mb-4"><div><img src="https://www.icone-png.com/png/3/2625.png" class="rounded-circle mr-1" alt="Chris Wood" width="40" height="40"><div class="text-muted small text-nowrap mt-2">'+strDate+'</div></div><div class="flex-shrink-1 bg-light rounded py-2 px-3 mr-3"><div class="font-weight-bold mb-1">Vous</div>'+msg+'</div></div>')        
                            socket.emit('messageSent1',{datasmsg : msg, datasuser : currentUser,datasalon:current_salon,datasdate:strDate});
                        break;

                        case 2:
                            e.preventDefault();
                            msg=$('#msgInput').val();
                            $("#messagesContent").append('<div class="chat-message-right mb-4"><div><img src="https://www.icone-png.com/png/3/2625.png" class="rounded-circle mr-1" alt="Chris Wood" width="40" height="40"><div class="text-muted small text-nowrap mt-2">'+strDate+'</div></div><div class="flex-shrink-1 bg-light rounded py-2 px-3 mr-3"><div class="font-weight-bold mb-1">Vous</div>'+msg+'</div></div>')        
                            socket.emit('messageSent2',{datasmsg : msg, datasuser : currentUser,datasalon:current_salon,datasdate:strDate});
                        break;

                        case 3:
                            e.preventDefault();
                            msg=$('#msgInput').val();
                            $("#messagesContent").append('<div class="chat-message-right mb-4"><div><img src="https://www.icone-png.com/png/3/2625.png" class="rounded-circle mr-1" alt="Chris Wood" width="40" height="40"><div class="text-muted small text-nowrap mt-2">'+strDate+'</div></div><div class="flex-shrink-1 bg-light rounded py-2 px-3 mr-3"><div class="font-weight-bold mb-1">Vous</div>'+msg+'</div></div>')        
                            socket.emit('messageSent3',{datasmsg : msg, datasuser : currentUser,datasalon:current_salon,datasdate:strDate});
                        break;

                        case 4:
                            e.preventDefault();
                            msg=$('#msgInput').val();
                            $("#messagesContent").append('<div class="chat-message-right mb-4"><div><img src="https://www.icone-png.com/png/3/2625.png" class="rounded-circle mr-1" alt="Chris Wood" width="40" height="40"><div class="text-muted small text-nowrap mt-2">'+strDate+'</div></div><div class="flex-shrink-1 bg-light rounded py-2 px-3 mr-3"><div class="font-weight-bold mb-1">Vous</div>'+msg+'</div></div>')        
                            socket.emit('messageSent4',{datasmsg : msg, datasuser : currentUser,datasalon:current_salon,datasdate:strDate});
                        break;
                        default:
                            e.preventDefault();
                            alert("Vous n'avez pas selectionné de salon");
                    }
                });
              

//----------------------------------------------------------------------------------------
// Salon 1
//Lorsqu'on change de salon on clear la chatbox, on indique que le salon en cours est le 1, on demande l'historique des messages au serveur puis on l'affiche qaund il nous répond

                $('#salon1').submit(function(e) {
                    e.preventDefault();
                    var node = document.getElementById('messagesContent');
                    node.innerHTML = "";
                    current_salon=1;
                    socket.emit("ask1");
                });

                socket.on('messageRecive1',function (data)  {
                    var rUser = data.dataruser;
                    var rmsg = data.datarmsg;
                    var rsalon=data.datarsalon;
                    var rdate=data.datardate;
                    if (current_salon==rsalon){
                        $("#messagesContent").append('<div class="chat-message-left pb-4"><div><img src="https://www.icone-png.com/png/3/2625.png" class="rounded-circle mr-1" alt="Sharon Lessman" width="40" height="40"><div class="text-muted small text-nowrap mt-2">'+rdate+'</div></div><div class="flex-shrink-1 bg-light rounded py-2 px-3 ml-3"><div class="font-weight-bold mb-1">'+rUser+'</div>'+rmsg+'</div></div>')
                    }
                });
//----------------------------------------------------------------------------------------
// Salon 2
                $('#salon2').submit(function(e) {
                    e.preventDefault();
                    var node = document.getElementById('messagesContent');
                    node.innerHTML = "";
                    console.log("salon num: "+newchat);
                    current_salon=newchat;
                    socket.emit("ask"+newchat);
                });

                socket.on('messageRecive2',function (data)  {
                    var rUser = data.dataruser;
                    var rmsg = data.datarmsg;
                    var rsalon=data.datarsalon;
                    var rdate=data.datardate;
                    if (current_salon==rsalon){
                        $("#messagesContent").append('<div class="chat-message-left pb-4"><div><img src="https://www.icone-png.com/png/3/2625.png" class="rounded-circle mr-1" alt="Sharon Lessman" width="40" height="40"><div class="text-muted small text-nowrap mt-2">'+rdate+'</div></div><div class="flex-shrink-1 bg-light rounded py-2 px-3 ml-3"><div class="font-weight-bold mb-1">'+rUser+'</div>'+rmsg+'</div></div>')
                    }
                });

//----------------------------------------------------------------------------------------
// Salon 3
                $('#salon3').submit(function(e) {
                    e.preventDefault();
                    var node = document.getElementById('messagesContent');
                    node.innerHTML = "";
                    current_salon=3;
                    socket.emit("ask3");
                });

                socket.on('messageRecive3',function (data)  {
                    var rUser = data.dataruser;
                    var rmsg = data.datarmsg;
                    var rsalon=data.datarsalon;
                    var rdate=data.datardate;
                    if (current_salon==rsalon){
                        $("#messagesContent").append('<div class="chat-message-left pb-4"><div><img src="https://www.icone-png.com/png/3/2625.png" class="rounded-circle mr-1" alt="Sharon Lessman" width="40" height="40"><div class="text-muted small text-nowrap mt-2">'+rdate+'</div></div><div class="flex-shrink-1 bg-light rounded py-2 px-3 ml-3"><div class="font-weight-bold mb-1">'+rUser+'</div>'+rmsg+'</div></div>')
                    }
                });
//----------------------------------------------------------------------------------------
// Salon 4
                $('#salon4').submit(function(e) {
                    e.preventDefault();
                    var node = document.getElementById('messagesContent');
                    node.innerHTML = "";
                    current_salon=4;
                    socket.emit("ask4");
                });

                socket.on('messageRecive4',function (data)  {
                    var rUser = data.dataruser;
                    var rmsg = data.datarmsg;
                    var rsalon=data.datarsalon;
                    var rdate=data.datardate;
                    if (current_salon==rsalon){
                        $("#messagesContent").append('<div class="chat-message-left pb-4"><div><img src="https://www.icone-png.com/png/3/2625.png" class="rounded-circle mr-1" alt="Sharon Lessman" width="40" height="40"><div class="text-muted small text-nowrap mt-2">'+rdate+'</div></div><div class="flex-shrink-1 bg-light rounded py-2 px-3 ml-3"><div class="font-weight-bold mb-1">'+rUser+'</div>'+rmsg+'</div></div>')
                    }
                });

            });

        });
 
        </script>
    </body>
</html>
