var tID;
var background = document.getElementById("backgroundart");
var BGcontext = background.getContext("2d");
var character = document.getElementById("character");
var Characontext = character.getContext("2d");

var canvasDim = {
    width: background.width,
    height: background.height
};

var direction = {
    "ArrowUp": 0,
    "ArrowDown": 140,
    "ArrowLeft": 280,
    "ArrowRight": 420
};

var sy = direction["ArrowUp"];
var sx = 0;
var px = 0;
var py = 0;
var sprite = {
    city: null
}

var spriteLoaded = function () {

    this.loaded = true;

    for (let name in sprite) {
        if (!sprite[name].loaded) {
            return;
        }
    }
    init();
};

sprite.city = new Image();
sprite.city.src = "background.png";
sprite.city.onload = spriteLoaded;

function init () {
    for (let l = 0 ; l * 2800 <= canvasDim.height ; l++) {
        for (let c = 0 ; c * 1600 <= canvasDim.width ; c++) {
            BGcontext.drawImage(sprite.city, c * 2800, l * 1600, 1280, 900);
            animateScript();
        }
    }
}

var drawPikachu = function() {
    Characontext.drawImage(pikachu, sx, sy, 140, 140, px, py, 140, 140);
    Characontext.fillText(currentUser,px+30,py);
    Characontext.textAlign = "centrer";
    Characontext.font = "bold 24px verdana, sans-serif"
    Characontext.fillStyle = "#FFFFFF";
};

var animateScript = function() {
    var position = 0;
    const diff = 140;
    const interval = 200;



    tID = setInterval ( () => {
        sx = position;
        if(sy<280){
            if(position < 560){
                position = position + diff;
            }
            else{
                position = 0;
            }
        }
        else{
            if(position < 280){
                position = position + diff;
            }
            else{
                position = 0;
            }
        }
        Characontext.clearRect(px, py, 140, 140);
        
        drawPikachu();
    }, interval );
}



var pikachu = new Image();
pikachu.src = "RedAnime.png";
pikachu.height = 280;
pikachu.width = 350;
pikachu.onload = animateScript;

document.onkeydown = function (e) {
    if (direction[e.key] === undefined) {
        return;
    }
    sy = direction[e.key];
    var ppx=px;
    var ppy=py;
    if(sy==420) { px = px + 10;} else if(sy==280) {px = px - 10;} else if(sy==0) {py = py - 10;} else if(sy==140) {py = py + 10;}
    Characontext.clearRect(ppx, ppy-20, 140, 200);
    drawPikachu();
};
animateScript();