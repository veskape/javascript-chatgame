# Projet_JS_Bacchis_Epale

requis: nodejs

Pour lancer le projet:
- se mettre dans le dossier du projet (cd)
- npm install
- node server.js

Nous avons en premier réalisé l'application de chat comme une application classique (connexion, plusieurs salons). Puis nous avons essayé d'intégrer la dimension jeu à ce qui avait déjà été fait. Cela explique les restes de codes sur les salons qui peuvent être réutilisés pour créer des salons privés.

Nous n'avons malheureusement pas pu réaliser toutes les fonctions demandées en temps donné (nous avons eu beaucoup de difficultés à venir à bout de ce qu'on a déjà)
manquant:
- changement de personnage (commencé mais pas terminé pour cause de bug / impossible de mettre une valeur d'une liste en temps que chemin pour l'image)
- message d'acceptation ou non de demande de messagerie
- canal privé (réalisé en grande partie, un bug de renversement de tableau a bloqué la réalisation totale). les messages s'envoient réellement en privé (test avec 3 fenetres)
